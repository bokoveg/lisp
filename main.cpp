#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <list>
#include <map>
#include <algorithm>

std::string str(long n);

bool isdig(char c);

enum cell_type { Symbol, Number, List, Proc, Boolean, Error };

struct environment;
struct cell {
    typedef cell(*proc_type)(const std::vector<cell> &);
    typedef std::vector<cell>::const_iterator iter;
    typedef std::map<std::string, cell> map;
    std::string error_msg;
    cell_type type;
    std::string val;
    std::vector<cell> list;
    proc_type proc;
    environment * env;
    cell(cell_type type = Symbol) : type(type), env(0) {}
    cell(cell_type type, const std::string val) : type(type), val(val), env(0) {}
    cell(proc_type proc) : type(Proc), proc(proc), env(0) {}
};

const cell false_sym(Boolean, "#f");
const cell true_sym(Boolean, "#t");
const cell nil(Symbol, "nil");
const cell syntax_error(Error, "syntax error");
const cell runtime_error(Error, "runtime error");
const cell name_error(Error, "name error");

struct environment {
    environment(environment *outer = 0) : outer_(outer) {}

    typedef std::map<std::string, cell> map;

    map & find(const std::string & var) {
        return env_;
    }

    cell & operator[] (const std::string & var)
    {
        return env_[var];
    }

private:
    map env_;
    environment * outer_;
};


std::string str(long n) { std::ostringstream os; os << n; return os.str(); }

bool isdig(char c) { return isdigit(static_cast<unsigned char>(c)) != 0; }

void RaiseSyntaxError(std::string &s) {
    s = "syntax error";
}

void RaiseNameError(std::string &s) {
    s = "name error";
}

void RaiseRuntimeError(std::string &s) {
    s = "runtime error";
}


cell proc_add(const std::vector<cell> &c) {
    int64_t n = 0;
    for (auto it = c.begin(); it != c.end(); ++it) {
        if (it->type != Number) {
            return runtime_error;
        }
        n += atol(it->val.c_str());
    }
    return cell(Number, str(n));
}

cell proc_sub(const std::vector<cell> &c) {
    if (c.size() == 0) {
        return runtime_error;
    }
    if (c[0].type != Number) {
        return runtime_error;
    }
    int64_t n(atol(c[0].val.c_str()));
    for (auto it = c.begin() + 1; it != c.end(); ++it) {
        if (it->type == Number) {
            n -= atol(it->val.c_str());
        } else {
            return runtime_error;
        }
    }
    return cell(Number, str(n));
}

cell proc_mul(const std::vector<cell> &c) {
    int64_t n = 1;
    for (auto it = c.begin(); it != c.end(); ++it) {
        if (it->type == Number) {
            n *= atol(it->val.c_str());
        } else {
            return runtime_error;
        }
    }
    return cell(Number, str(n));
}

cell proc_div(const std::vector<cell> &c) {
    if (c.size() == 0) {
        return runtime_error;
    }
    if (c[0].type != Number) {
        return runtime_error;
    }
    int64_t n(atol(c[0].val.c_str()));
    for (auto it = c.begin() + 1; it != c.end(); ++it) {
        if (it->type == Number) {
            n /= atol(it->val.c_str());
        } else {
            return runtime_error;
        }
    }
    return cell(Number, str(n));
}

cell proc_greater(const std::vector<cell> & c) {
    if (c.size() == 0) {
        return true_sym;
    }
    if (c[0].type != Number) {
        return runtime_error;
    }
    int64_t n(atol(c[0].val.c_str()));
    for (auto it = c.begin() + 1; it != c.end(); ++it) {
        if (it->type != Number) {
            return runtime_error;
        }
        if (n <= atol(it->val.c_str()))
            return false_sym;
    }
    return true_sym;
}

cell proc_less(const std::vector<cell> & c)
{
    if (c.size() == 0) {
        return true_sym;
    }
    if (c[0].type != Number) {
        return runtime_error;
    }
    int64_t n(atol(c[0].val.c_str()));
    for (auto it = c.begin() + 1; it != c.end(); ++it) {
        if (it->type != Number) {
            return runtime_error;
        }
        if (n >= atol(it->val.c_str()))
            return false_sym;
    }
    return true_sym;
}

cell proc_less_equal(const std::vector<cell> & c) {
    if (c.size() == 0) {
        return true_sym;
    }
    if (c[0].type != Number) {
        return runtime_error;
    }
    int64_t n(atol(c[0].val.c_str()));
    for (auto it = c.begin() + 1; it != c.end(); ++it) {
        if (it->type != Number) {
            return runtime_error;
        }
        if (n > atol(it->val.c_str()))
            return false_sym;
    }
    return true_sym;
}

cell proc_equal(const std::vector<cell> &c) {
    if (c.size() == 0) {
        return true_sym;
    }
    if (c[0].type != Number) {
        return runtime_error;
    }
    int64_t n = atoi(c[0].val.c_str());
    for (auto it = c.begin() + 1; it != c.end(); ++it) {
        if (it->type != Number) {
            return runtime_error;
        }
        if (n != atoi(it->val.c_str())) {
            return false_sym;
        }
    }
    return true_sym;
}

cell proc_isnumber(const std::vector<cell> &c) {
    if (c.size() != 1) {
        return runtime_error;
    }
    if (c[0].type == Number) {
        return true_sym;
    } else {
        return false_sym;
    }
}

cell proc_greater_equal(const std::vector <cell> &c) {
    if (c.size() == 0) {
        return true_sym;
    }
    if (c[0].type != Number) {
        return runtime_error;
    }
    int64_t n(atol(c[0].val.c_str()));
    for (auto it = c.begin() + 1; it != c.end(); ++it) {
        if (it->type != Number) {
            return runtime_error;
        }
        if (n < atol(it->val.c_str())) {
            return false_sym;
        }
    }
    return true_sym;
}

cell proc_max(const std::vector<cell> &c) {

    if (c.size() == 0) {
        return runtime_error;
    }
    if (c[0].type != Number) {
        return runtime_error;
    }
    int64_t res = atoi(c[0].val.c_str());
    for (auto it = c.begin(); it != c.end(); ++it) {
        if (it->type != Number) {
            return runtime_error;
        }
        int64_t new_val = atoi(it->val.c_str());
        res = std::max(new_val, res);
    }
    return cell(Number, std::to_string(res));
}

cell proc_min(const std::vector<cell> &c) {

    if (c.size() == 0) {
        return runtime_error;
    }
    if (c[0].type != Number) {
        return runtime_error;
    }
    int64_t res = atoi(c[0].val.c_str());
    for (auto it = c.begin(); it != c.end(); ++it) {
        if (it->type != Number) {
            return runtime_error;
        }
        int64_t new_val = atoi(it->val.c_str());
        res = std::min(new_val, res);
    }
    return cell(Number, std::to_string(res));
}

cell proc_abs(const std::vector<cell> &c) {
    if (c.size() != 1) {
        return runtime_error;
    }
    if (c[0].type != Number) {
        return runtime_error;
    }
    int64_t value = abs(atoi(c[0].val.c_str()));
    return cell(Number, std::to_string(value));
}

cell proc_booleancheck(const std::vector<cell> &c) {
    if (c.size() != 1) {
        return runtime_error;
    }
    if (c[0].type == Boolean) {
        return true_sym;
    } else {
        return false_sym;
    }
}

cell proc_not(const std::vector<cell> &c) {
    if (c.size() != 1) {
        return runtime_error;
    }
    if (c[0].type != Boolean) {
        return false_sym;
    }
    if (c[0].val == "#f") {
        return true_sym;
    }
    if (c[0].val == "#t") {
        return false_sym;
    }
    return false_sym;
}

cell proc_and(const std::vector<cell> &c) {
    if (c.size() == 0) {
        return true_sym;
    }
    cell last = true_sym;
    for (auto it = c.begin(); it != c.end(); ++it) {
        if (it->type != Boolean) {
            last = *it;
        } else {
            if (it->val == "#f") {
                return false_sym;
            }
        }
    }
    return last;
}

cell proc_or(const std::vector<cell> &c) {
    if (c.size() == 0) {
        return false_sym;
    }
    for (auto it = c.begin(); it != c.end(); ++it) {
        if (it->type != Boolean) {
            return it->type;
        } else {
            if (it->val == "#t") {
                return true_sym;
            }
        }
    }
    return false_sym;
}

cell proc_issymbol(const std::vector<cell> &c) {
    if (c.size() != 1) {
        return runtime_error;
    } else {
        if (c[0].type == Symbol) {
            return true_sym;
        } else {
            return false_sym;
        }
    }
}

cell proc_ispair(const std::vector<cell> &c) {
    if (c.size() != 1) {
        return runtime_error;
    }
    if (c[0].type != List) {
        return false_sym;
    }
    if (c[0].list.size() == 2) {
        return true_sym;
    }
    if (c[0].list.size() == 3 && c[0].list[1].val == ".") {
        return true_sym;
    }
    return false_sym;
}

cell proc_isnull(const std::vector<cell> &c) {
    if (c.size() != 1) {
        return runtime_error;
    }
    if (c[0].type != List) {
        return false_sym;
    }
    if (c[0].list.size() == 0) {
        return true_sym;
    }
    return false_sym;
}

cell proc_islist(const std::vector<cell> &c) {
    if (c.size() != 1) {
        return runtime_error;
    }
    if (c[0].type != List) {
        return false_sym;
    }
    for (auto elem : c[0].list) {
        if (elem.val == ".") {
            return false_sym;
        }
    }
    return true_sym;
}

cell proc_cons(const std::vector<cell> &c) {
    if (c.size() != 2) {
        return runtime_error;
    }
    cell res(List, "");
    res.list.push_back(c[0]);
    res.list.push_back(cell(Symbol, "."));
    res.list.push_back(c[1]);
    return res;
}

cell proc_car(const std::vector<cell> &c) {
    if (c.size() != 1) {
        return runtime_error;
    }
    if (proc_ispair(c).val != true_sym.val) {
        return runtime_error;
    }
    return c[0].list[0];
}

cell proc_cdr(const std::vector<cell> &c) {
    if (c.size() != 1) {
        return runtime_error;
    }
    if (proc_ispair(c).val != true_sym.val) {
        return runtime_error;
    }
    if (c[0].list.size() == 2) {
        return c[0].list[1];
    }
    if (c[0].list.size() == 3) {
        return c[0].list[2];
    }
}

cell proc_list(const std::vector<cell> &c) {
    cell res(List, "");
    for (auto i : c) {
        res.list.push_back(i);
    }
    return res;
}

cell proc_listref(const std::vector<cell> &c) {
    if (c.size() != 2) {
        return syntax_error;
    }
    if (c[0].type != List) {
        return runtime_error;
    }
    if (c[1].type != Number) {
        return runtime_error;
    }
    auto index = atoi(c[1].val.c_str());
    if (index < 0 || index > c[0].list.size() - 1) {
        return runtime_error;
    } else {
        return c[0].list[index];
    }
}

cell proc_listtail(const std::vector<cell> &c) {
    cell res(List, "");
    if (c.size() != 2) {
        return syntax_error;
    }
    if (c[1].type != Number) {
        return runtime_error;
    }
    auto index = atoi(c[1].val.c_str());
    if (index < 0 || index > c[0].list.size()) {
        return runtime_error;
    } else {
        for (int i = index; i < c[0].list.size(); ++i) {
            res.list.push_back(c[0].list[i]);
        }
    }
    return res;
}

void add_globals(environment & env) {
    env["nil"] = nil;
    env["#f"] = false_sym;
    env["#t"] = true_sym;
    env["boolean?"] = cell(&proc_booleancheck);
    env["number?"] = cell(&proc_isnumber);
    env["not"] = cell(&proc_not);
    env["and"] = cell(&proc_and);
    env["or"] = cell(&proc_or);
    env["+"] = cell(&proc_add);
    env["-"] = cell(&proc_sub);
    env["*"] = cell(&proc_mul);
    env["/"] = cell(&proc_div);
    env[">"] = cell(&proc_greater);
    env["<"] = cell(&proc_less);
    env[">="] = cell(&proc_greater_equal);
    env["<="] = cell(&proc_less_equal);
    env["="] = cell(&proc_equal);
    env["min"] = cell(&proc_min);
    env["max"] = cell(&proc_max);
    env["abs"] = cell(&proc_abs);
    env["symbol?"] = cell(&proc_issymbol);
    env["pair?"] = cell(&proc_ispair);
    env["null?"] = cell(&proc_isnull);
    env["list?"] = cell(&proc_islist);
    env["cons"] = cell(&proc_cons);
    env["car"] = cell(&proc_car);
    env["cdr"] = cell(&proc_cdr);
    env["list"] = cell(&proc_list);
    env["list-ref"] = cell(&proc_listref);
    env["list-tail"] = cell(&proc_listtail);
}

cell ListReduce(cell x) {
    if (x.type != List) {
        return x;
    }
    cell res(List, "");
    bool has_dot = false;
    for (int i = 0; i < x.list.size(); ++i) {
        if (x.list[i].val == ".") {
            has_dot = true;
            break;
        }
    }
    if (x.type == List && has_dot) {
        if (x.list.size() >= 3) {
            if (x.list[x.list.size() - 2].val == ".") {
                for (int i = 0; i < x.list.size() - 2; ++i) {
                    res.list.push_back(x.list[i]);
                }
                if (x.list[x.list.size() - 1].type == List) {
                    for (auto i : x.list[x.list.size() - 1].list) {
                        res.list.push_back(i);
                    }
                } else {
                    res.list.push_back(x.list[x.list.size() - 2]);
                    res.list.push_back(x.list[x.list.size() - 1]);
                }
                return res;
            } else {
                return syntax_error;
            }
        } else {
            return syntax_error;
        }
    } else {
        return x;
    }
}

cell eval(cell x, environment * env)
{
    if (x.type == Error) {
        return x;
    }
    if (x.type == Boolean) {
        return x;
    }
    if (x.type == Symbol) {
        auto res = env->find(x.val);
        if (res.find(x.val) == res.end()) {
            return name_error;
        } else {
            return res[x.val];
        }
    }
    if (x.type == Number) {
        return x;
    }
    if (x.list.empty()) {
        return runtime_error;
    }
    bool has_dot = false;
    for (int i = 0; i < x.list.size(); ++i) {
        if (x.list[i].val == ".") {
            has_dot = true;
        }
    }

    if (x.list[0].type == Symbol) {
        if (x.list[0].val == "quote") {
            return x.list[1];
        }
        if (x.list[0].val == "if") {
            if (x.list.size() < 3 || x.list.size() > 4) {
                return syntax_error;
            }
            auto if_res = eval(x.list[1], env).val;
            cell next;
            if (if_res == "#f") {
                if (x.list.size() < 4) {
                    next = cell(List, "");
                } else {
                    next = eval(x.list[3], env);
                }
            } else {
                next = eval(x.list[2], env);
            }
            return next;
        }
        if (x.list[0].val == "set!") {
            if (x.list.size() != 3) {
                return syntax_error;
            }
            auto res = env->find(x.list[1].val);
            if (res.find(x.list[1].val) == res.end()) {
                return name_error;
            } else {
                return (*env)[x.list[1].val] = eval(x.list[2], env);
            }
        }
        if (x.list[0].val == "set-car!" || x.list[0].val == "set-cdr!") {
            if (x.list.size() != 3) {
                return syntax_error;
            }
            auto res = env->find(x.list[1].val);
            if (res.find(x.list[1].val) == res.end()) {
                return name_error;
            } else {
                const std::vector<cell> val({ res[x.list[1].val] });

                if (proc_ispair(val).val != true_sym.val) {
                    return runtime_error;
                }
                cell first_val, second_val;
                if (x.list[0].val == "set-car!") {
                    first_val = eval(x.list[2], env);
                    second_val = proc_cdr(val);
                } else {
                    second_val = eval(x.list[2], env);
                    first_val = proc_cdr(val);
                }
                const std::vector<cell> res({ first_val, second_val });
                auto t = proc_cons(res);
                (*env)[x.list[1].val] = t;
                return{ Symbol, "" };
            }
        }
        if (x.list[0].val == "define") {
            if (x.list.size() != 3) {
                return syntax_error;
            }
            return (*env)[x.list[1].val] = eval(x.list[2], env);
        }
        if (x.list[0].val == "begin") {
            for (size_t i = 1; i < x.list.size() - 1; ++i)
                eval(x.list[i], env);
            return eval(x.list[x.list.size() - 1], env);
        }
    }
    cell proc(eval(x.list[0], env));
    if (proc.type == Error) {
        return proc;
    }
    std::vector<cell> exps;
    for (cell::iter exp = x.list.begin() + 1; exp != x.list.end(); ++exp) {
        auto res = eval(*exp, env);
        if (res.type == Error) {
            return res;
        }
        if (x.list[0].val == "and" && res.type == Boolean && res.val == "#f") {
            return false_sym;
        }
        if (x.list[0].val == "or" && (res.type != Boolean || res.val == "#t")) {
            return res;
        }
        exps.push_back(res);
    }
    if (proc.type == Proc) {
        return proc.proc(exps);
    }

    return runtime_error;
}

bool check_brackets(std::list<std::string>& tokens) {
    int balance = 0;
    for (auto token : tokens) {
        if (token == "(") {
            ++balance;
        }
        if (token == ")") {
            --balance;
        }
        if (balance < 0) {
            return false;
        }
    }
    if (balance == 0) {
        return true;
    } else {
        return false;
    }
}

std::list<std::string> tokenize(const std::string &str)
{
    std::list<std::string> tokens;
    std::string s;
    for (size_t i = 0; i < str.length() + 1; ++i) {
        if (str[i] == '(' || str[i] == ')' || str[i] == ' ' || i == str.length() || str[i] == '\'') {
            if (s.length() > 0) {
                tokens.push_back(s);
                s = "";
            }
            if (str[i] == ')') {
                tokens.push_back(")");
            }
            if (str[i] == '(') {
                tokens.push_back("(");
            }
            if (str[i] == '\'') {
                tokens.push_back("\'");
            }
        } else {
            s += str[i];
        }
    }
    if (!check_brackets(tokens)) {
        return{ "(", ".", ")" };
    }
    return tokens;
}

cell atom(const std::string & token) {
    if (token == "#f" || token == "#t") {
        return cell(Boolean, token);
    }
    if (isdig(token[0]) || (token[0] == '-' && isdig(token[1]))) {
        return cell(Number, token);
    }
    if (token[0] == '+' && isdig(token[1])) {
        std::string s = token;
        s.erase(0, 1);
        return cell(Number, s);
    }
    return cell(Symbol, token);
}

cell read_from(std::list<std::string> &tokens) {
    const std::string token(tokens.front());
    tokens.pop_front();
    if (token == "\'") {
        cell c(List, "");
        c.list.push_back(cell(Symbol, "quote"));
        c.list.push_back(read_from(tokens));
        return c;
    }
    if (token == "(") {
        cell c(List);
        while (tokens.front() != ")")
            c.list.push_back(read_from(tokens));
        tokens.pop_front();
        return ListReduce(c);
    } else {
        return atom(token);
    }
}

cell read(const std::string & s) {
    std::list<std::string> tokens(tokenize(s));
    return read_from(tokens);
}

std::string to_string(const cell & exp) {
    if (exp.type == List) {
        std::string s("(");
        for (cell::iter e = exp.list.begin(); e != exp.list.end(); ++e)
            s += to_string(*e) + ' ';
        if (s[s.size() - 1] == ' ')
            s.erase(s.size() - 1);
        return s + ')';
    } else if (exp.type == Proc)
        return "<Proc>";
    else
        return exp.val;
}

void repl(environment * env) {
    while (true) {
        std::string line;
        std::getline(std::cin, line);
        if (!std::cin) {
            exit(0);
        }
        std::cout << to_string(eval(read(line), env)) << '\n';
    }
}


int main() {
    environment env;
    add_globals(env);
    repl(&env);
}
